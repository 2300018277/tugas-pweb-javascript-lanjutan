document.getElementById('biodataForm').addEventListener('submit', function(event) {
    event.preventDefault();

    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const phone = document.getElementById('phone').value;
    const address = document.getElementById('address').value;
    
    // Validasi sederhana
    if(name === '' || email === '' || phone === '' || address === '') {
        alert('Semua field harus diisi!');
        return;
    }

    // Validasi email
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailPattern.test(email)) {
        alert('Email tidak valid!');
        return;
    }

    // Validasi nomor telepon
    const phonePattern = /^[0-9]{10,}$/;
    if (!phonePattern.test(phone)) {
        alert('Nomor telepon tidak valid!');
        return;
    }

    // Jika valid, tampilkan hasil biodata
    const result = document.getElementById('result');
    result.innerHTML = `
        <h2>Hasil Biodata</h2>
        <p><strong>Nama:</strong> ${name}</p>
        <p><strong>Email:</strong> ${email}</p>
        <p><strong>Nomor Telepon:</strong> ${phone}</p>
        <p><strong>Alamat:</strong> ${address}</p>
    `;
});
